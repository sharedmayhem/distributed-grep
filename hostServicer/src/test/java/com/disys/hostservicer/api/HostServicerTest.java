package com.disys.hostservicer.api;

import com.disys.common.HttpClient;
import com.disys.common.InMemoryLogAppender;
import com.disys.hostservicer.service.LogServicerLocator;
import com.google.common.collect.Lists;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;

import static com.disys.common.model.Target.target;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HostServicerTest {

    {
        System.setProperty("java.util.logging.manager","org.apache.logging.log4j.jul.LogManager");
    }

    @Mock
    private HttpClient httpClient;

    @Mock
    private LogServicerLocator logServicerLocator;

    @Test
    public void ifNoLogServicersAreOnline_thenLetUserKnow() throws IOException {
        when(logServicerLocator.targets()).thenReturn(new ArrayList<>());
        Response response = new HostServicer(logServicerLocator, httpClient).grep("dep", "pom.xml", Lists.newArrayList("-i"));
        assertThat(getOutput(response), Matchers.equalTo("No log servicers available"));
    }

    @Test
    public void ifMultipleLogServicersAreOnline_thenStreamBothResponsesOneByOne() throws IOException {
        Response response1 = inboundResponse("This is from test1", "test1");
        Response response2 = inboundResponse("This is from test2", "test2");
        when(logServicerLocator.targets()).thenReturn(Lists.newArrayList(target("http://test1"),target("http://test2")));
        when(httpClient.request(eq(URI.create("http://test1")), anyMap(), any(), any())).thenReturn(response1);
        when(httpClient.request(eq(URI.create("http://test2")), anyMap(), any(), any())).thenReturn(response2);

        Response response = new HostServicer(logServicerLocator, httpClient).grep("dep", "pom.xml", Lists.newArrayList("-i"));
        assertThat(getOutput(response), Matchers.equalTo("test1 --> This is from test1\ntest2 --> This is from test2\n"));
    }

    @Test
    public void ifLogServicerFailes_thenFail() {
        Client client = mock(Client.class);
        HttpClient httpClient = new HttpClient(client);
        when(client.target(any(URI.class))).thenThrow(new RuntimeException("Cannot reach host"));
        when(logServicerLocator.targets()).thenReturn(Lists.newArrayList(target("http://test1")));

        new HostServicer(logServicerLocator, httpClient).grep("dep", "pom.xml", Lists.newArrayList("-i"));

        assertThat(InMemoryLogAppender.lastLog(), Matchers.equalTo("Cannot reach host"));
    }


    private Response inboundResponse(String content, String sourceSystem) {
        Response mock = mock(Response.class);
        when(mock.hasEntity()).thenReturn(true);
        when(mock.readEntity(any(Class.class))).thenReturn(new ByteArrayInputStream((content).getBytes()));
        when(mock.getHeaderString("source")).thenReturn(sourceSystem);
        return mock;
    }

    private java.lang.String getOutput(Response response) throws IOException {
        StreamingOutput streamingOutput = (StreamingOutput) response.getEntity();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        streamingOutput.write(byteArrayOutputStream);

        return byteArrayOutputStream.toString();
    }

}