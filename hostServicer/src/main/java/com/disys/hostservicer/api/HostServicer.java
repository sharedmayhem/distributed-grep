package com.disys.hostservicer.api;

import com.disys.common.ArgumentMapBuilder;
import com.disys.common.HttpClient;
import com.disys.common.IOStreamHelper;
import com.disys.common.model.Paths;
import com.disys.common.model.Target;
import com.disys.hostservicer.service.LogServicerLocator;
import org.apache.logging.log4j.jul.ApiLogger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.InputStream;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static com.disys.common.Validator.noBlanks;

@Path(Paths.HostServicer.HOST_SERVICER)
@Produces(MediaType.TEXT_PLAIN)
@Consumes(MediaType.TEXT_PLAIN)
public class HostServicer {
    private static final Logger LOGGER = ApiLogger.getLogger(HostServicer.class.getName());
    private LogServicerLocator logServicerLocator ;
    private HttpClient httpClient ;

    public HostServicer() {
        this(new LogServicerLocator(), new HttpClient());
    }

    public HostServicer(LogServicerLocator logServicerLocator, HttpClient httpClient) {
        this.logServicerLocator = logServicerLocator;
        this.httpClient = httpClient;
    }

    @GET
    @Path(Paths.HostServicer.GREP)
    public Response grep(@QueryParam("pattern") String pattern, @QueryParam("fileLocation") String fileLocation, @QueryParam("grepOption") List<String> grepOptions) {
        noBlanks().apply(() -> "Pattern cannot be null", pattern);
        noBlanks().apply(() -> "File location cannot be null", fileLocation);

        Map<String, List<String>> arguments = ArgumentMapBuilder
                .start()
                .add("pattern", pattern)
                .add("fileLocation", fileLocation)
                .add("grepOption", grepOptions)
                .build();
        List<Target> targets = logServicerLocator.targets();

        List<Response> responses = targets.parallelStream()
                .map(target -> httpClient.request(
                        target.baseURI(), arguments,
                        ex -> {
                            LOGGER.severe(ex.getMessage());
                            return Response.status(Response.Status.NOT_FOUND).header("source", target.host()).build();
                        },
                        Paths.LogServicer.LOG_SERVICER,
                        Paths.LogServicer.GREP))
                .collect(Collectors.toList());


        return Response.ok(getStreamingOutput(responses)).build();
    }

    private StreamingOutput getStreamingOutput(List<Response> responses) {
        return output -> {
            try {
                if (responses.isEmpty()) {
                    output.write("No log servicers available".getBytes());
                } else {
                    for (Response response : responses) {
                        if (response.hasEntity()) {
                            String source = response.getHeaderString("source");
                            IOStreamHelper.inToOut(
                                    response.readEntity(InputStream.class),
                                    output,
                                    input -> source + " --> " + input);
                        }
                    }
                }
            } finally {
                output.flush();
                output.close();
            }
        };
    }

    @POST
    @Path(Paths.HostServicer.HEARTBEAT)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void heartbeat(@FormParam("url") String url) {
        noBlanks().apply(() -> "URL cannot be null", url);
        logServicerLocator.heartbeat(URI.create(url));
    }

    @GET
    @Path(Paths.HostServicer.LIST)
    public Response list() {
        List<Target> targets = logServicerLocator.targets();
        StringBuilder stringBuilder = new StringBuilder();
        if (targets.size() > 0) {
            targets.forEach(target -> stringBuilder.append(target.baseURI().toASCIIString()).append("\n"));
        } else {
            stringBuilder.append("No Log Servicers are registered at the moment");
        }
        return Response.ok().type(MediaType.TEXT_PLAIN_TYPE).entity(stringBuilder.toString()).build();
    }

}
