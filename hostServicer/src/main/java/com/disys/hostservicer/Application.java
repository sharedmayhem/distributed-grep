package com.disys.hostservicer;

import com.disys.common.DebugExceptionMapper;
import com.disys.common.GenericApplication;
import com.disys.common.model.Paths;
import com.disys.hostservicer.api.HostServicer;
import org.apache.logging.log4j.jul.ApiLogger;
import org.glassfish.grizzly.http.server.HttpServer;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Application {


    private static final Logger LOGGER = ApiLogger.getLogger(Application.class.getName());

    public static void main(String[] args) {
        try {
            HttpServer httpServer = new GenericApplication().get(Paths.HostServicer.BASE, HostServicer.class, DebugExceptionMapper.class);
            LOGGER.info("Server started at: " + Paths.HostServicer.BASE);
            httpServer.start();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Could not start the server", e);
        }
    }
}
