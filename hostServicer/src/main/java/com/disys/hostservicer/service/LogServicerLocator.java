package com.disys.hostservicer.service;

import com.disys.common.model.Target;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.apache.logging.log4j.jul.ApiLogger;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class LogServicerLocator {
    private static final Logger LOGGER = ApiLogger.getLogger(LogServicerLocator.class.getName());

    private static final Cache<URI, Long> hostVSLastHeartbeat = CacheBuilder.newBuilder()
            .maximumSize(10)
            .expireAfterWrite(10, TimeUnit.SECONDS)
            .removalListener(notification -> LOGGER.warning(notification.getKey() + " log servicer has: " + notification.getCause() + ". Last published heartbeat: " + new Date((Long) notification.getValue())))
            .build();

    public List<Target> targets() {
        return hostVSLastHeartbeat
                .asMap()
                .entrySet()
                .stream()
                .map(entry -> Target.target(entry.getKey()))
                .collect(Collectors.toList());
    }

    public void heartbeat(URI host) {
        hostVSLastHeartbeat.put(host, System.currentTimeMillis());
    }
}
