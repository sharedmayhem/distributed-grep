# README #

### What is this repository for? ###

* ###### Quick summary
    * This project greps a search pattern across one or more distributed servers. This project is Java based and uses a REST web server implemented by Grizzly and Jersey. 
* ###### Version
    * beta

### How do I get set up? ###
* ###### Dependencies
    * Java 8
    * Maven
    * Bash
    * AWS (if you want to use the provisioning scripts)
* ###### Set up and run
    * Clone project
    * mvn clean package
    * Navigate to scripts directory at the root of the project
    * Open file *machines.txt* and setup machine types (Jump host, Host Servicer and Log Servicer)
    * Run *provision.sh* by providing the AWS pem file as an argument
    * At this point, the newly built libraries are deployed on the AWS infrastructure and all your host servicers and log servicers are up and running
* ###### How to run tests
    * mvn clean install 

### Design ###
* ##### Actors
    * Log servicers are a group of web applications that grep a local log file for an input pattern
    * Host servicers are a group of web applications that query all registered log servicers for an input pattern
    * Amazon's Elastic Load Balancer is used to load balance and provide high availability access to the host servicers
* ##### Access
    * Users can access host servicers only via the load balancer and not directly because the host servicers do not have a public IP
    * Users cannot access log servicers because they exist in a private subnet, accessible only to the host servicers and the jump host
    * Only the jump host has a public IP and SSH enabled from outside Amazon's VPC. All other hosts can be accessed via SSH only from the jump host
* ##### Group Membership
    * All log servicers send periodic heartbeats (every 5 seconds) to the host servicer to indicate their prescence
    * Host servicers will forward user queries to only registered log servicers. Log servicers that have not published a heartbeat in the last 10 seconds will be removed from the registered list of log servicers
    * A log service, upon restart, will automatically register itself with the host servicers
* ##### Memory Management
    * The log servicers do not build an in-memory representation of grep results. Instead they stream out the results to the host servicer
    * The host servicers also do not build an in-memory representation of grep results. They collect the result streams from multiple log servicers and stream out the results. The results are streamed out one by one in the interests of obtaining a contiguous output i.e., output from a particular log servicer is displayed together
* ##### Deployment Architecture   
![Alt text](https://bitbucket.org/adibar/distributed-grep/downloads/Deployment.jpg)
### Further improvements ###
* Robust membership algorithm
* Caching of frequently used grep patterns
* Automated integration tests
* Rid code duplication in provisioning scripts
* Automated provisioning in AWS via Maven
* GUI to perform search queries and choose specific hosts


### Who do I talk to? ###

* Repo owner

