package com.disys.commandservicer;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class CommandServicer {

    public InputStream grep(String pattern, String fileLocation, List<String> grepOptions) throws IOException {
        List<String> command = new ArrayList<>();
        command.add("grep");
        grepOptions.forEach(opt -> {
            if (StringUtils.isNotBlank(opt)) {
                command.add(opt);
            }
        });
        command.add(pattern);
        command.add(fileLocation);
        return runProcess(command);
    }

    private InputStream runProcess(List<String> command) throws IOException {
        ProcessBuilder processBuilder = new ProcessBuilder(command).redirectErrorStream(true);
        Process process = processBuilder.start();
        return process.getInputStream();
    }

}
