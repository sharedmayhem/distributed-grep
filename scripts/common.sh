#!/bin/bash

function do_ssh() {
    ssh -n -i $pem_file ec2-user@$host -oStrictHostKeyChecking=no $1
}

function do_scp() {
    scp -i $pem_file -r $1 ec2-user@$host:$2 >> scp_output.log 2>&1
}

function kill_java_processes(){
    do_ssh "ps -ef | grep -i java | grep -v grep | awk '{ print \"kill \" \$2 }'|/bin/sh"
}