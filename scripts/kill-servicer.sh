#!/bin/sh
pem_file="distributed-grep.pem.txt"

# *** Variables 'pem_file' and 'host' declared in this script are used in common.sh ***
. ./common.sh

host=$1
echo "Killing servicer on host: $host"
kill_java_processes


