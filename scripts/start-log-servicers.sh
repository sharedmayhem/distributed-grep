#!/bin/sh
file="machines.txt"
pem_file="distributed-grep.pem.txt"

# *** Variables 'pem_file' and 'host' declared in this script are used in common.sh ***
. ./common.sh

while IFS= read -r line
do
    IFS=':' read -ra machine_info <<< "$line"
    machine_type=${machine_info[0]}
    host=${machine_info[1]}
    if [ $machine_type = 'LogServicer' ]; then
        echo "Provisioning host: $host"
        kill_java_processes
        do_ssh 'rm -rf *.jar'
        do_scp 'binaries/logServicer*.jar' '/home/ec2-user'
        do_ssh 'nohup java -Djava.util.logging.manager=org.apache.logging.log4j.jul.LogManager -jar /home/ec2-user/logServicer*.jar http://10.0.2.33:8090 http://10.0.1.7:8090 &>/dev/null &'
        echo "Log servicer started on host: $host"
    fi
done <"$file"


