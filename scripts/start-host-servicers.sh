#!/bin/sh
file="machines.txt"
pem_file="distributed-grep.pem.txt"

# *** Variables 'pem_file' and 'host' declared in this script are used in common.sh ***
. ./common.sh

while IFS= read -r line
do
    IFS=':' read -ra machine_info <<< "$line"
    machine_type=${machine_info[0]}
    host=${machine_info[1]}
    if [ $machine_type = 'HostServicer' ]; then
        echo "Provisioning host: $host"
        kill_java_processes
        do_ssh 'rm -rf *.jar'
        do_scp 'binaries/hostServicer*.jar' '/home/ec2-user'
        do_ssh 'nohup java -Djava.util.logging.manager=org.apache.logging.log4j.jul.LogManager -jar /home/ec2-user/hostServicer*.jar &>/dev/null &'
        echo "Host servicer started on host: $host"
    fi
done <"$file"


