#!/bin/sh
file="machines.txt"
pem_file="distributed-grep.pem.txt"

# *** Variables 'pem_file' and 'host' declared in this script are used in common.sh ***
. ./common.sh

while IFS= read -r line
do
    IFS=':' read -ra machine_info <<< "$line"
    machine_type=${machine_info[0]}
    host=${machine_info[1]}
    if [ $machine_type = 'LogServicer' ]; then
        echo "Killing log servicer on host: $host"
        kill_java_processes
    fi
done <"$file"


