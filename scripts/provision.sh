#!/bin/bash
pem_file=$1
file="machines.txt"

# *** Variables 'pem_file' and 'host' declared in this script are used in common.sh ***
. ./common.sh

rm *.log

while IFS= read -r line
do
    IFS=':' read -ra machine_info <<< "$line"
    if [ "${machine_info[0]}" = 'JumpHost' ]; then
        host=${machine_info[1]}
	    echo "Provisioning jump host:" $host

	    do_ssh 'rm -rf binaries'
	    do_ssh 'mkdir binaries'
        do_scp '../resources/*.jar' '/home/ec2-user/binaries'
        echo "Copied binaries"

        do_scp 'machines.txt' '/home/ec2-user'
        echo "Copied machine descriptions"

	    do_scp 'provision-cluster.sh' '/home/ec2-user'
        do_ssh 'chmod +x /home/ec2-user/provision-cluster.sh'

        do_scp 'common.sh' '/home/ec2-user'
        do_ssh 'chmod +x /home/ec2-user/common.sh'

        do_scp 'kill-host-servicers.sh' '/home/ec2-user'
        do_ssh 'chmod +x /home/ec2-user/kill-host-servicers.sh'

        do_scp 'kill-log-servicers.sh' '/home/ec2-user'
        do_ssh 'chmod +x /home/ec2-user/kill-log-servicers.sh'

        do_scp 'kill-servicer.sh' '/home/ec2-user'
        do_ssh 'chmod +x /home/ec2-user/kill-servicer.sh'

        do_scp 'start-host-servicers.sh' '/home/ec2-user'
        do_ssh 'chmod +x /home/ec2-user/start-host-servicers.sh'

        do_scp 'start-log-servicers.sh' '/home/ec2-user'
        do_ssh 'chmod +x /home/ec2-user/start-log-servicers.sh'

        echo "Copied cluster provisioning script"

        do_scp $pem_file '/home/ec2-user'
        do_ssh 'chmod 400 *.pem.txt'
        echo "Copied pem file"

        do_ssh '. /home/ec2-user/provision-cluster.sh > cluster.log 2>&1'
        echo "Provisioned clusters"

    fi

done <"$file"


