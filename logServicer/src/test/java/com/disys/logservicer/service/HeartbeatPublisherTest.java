package com.disys.logservicer.service;

import com.disys.common.HttpClient;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;
import java.util.stream.Stream;

import static com.disys.common.InMemoryLogAppender.lastLog;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class HeartbeatPublisherTest {

    @Mock
    private List<URI> hostServicers;

    @Mock
    private HttpClient httpClient;

    @Mock
    private Client client;

    @Test
    public void ifHeartbeatPublishFails_thenLogError() {
        when(hostServicers.stream()).thenReturn(Stream.of(URI.create("http://test")));
        when(client.target(any(URI.class))).thenThrow(new RuntimeException("Cannot reach host servicer"));

        new HeartbeatPublisher(hostServicers, new HttpClient(client)).publishHeartbeat();


        assertThat(lastLog(), Matchers.equalTo("Could not publish heartbeat because: Status code: 400  Status reason: Cannot reach host servicer"));
    }

    @Test
    public void ifHeartbeatPublishSucceeds_thenResponseIsClosed() {
        Response response = mock(Response.class);
        when(response.getStatus()).thenReturn(200);
        when(hostServicers.stream()).thenReturn(Stream.of(URI.create("http://test")));
        when(httpClient.post(any(), any(), any(), any())).thenReturn(response);

        new HeartbeatPublisher(hostServicers, httpClient).publishHeartbeat();

        verify(response).close();
    }

    private Response response(int statusCode) {
        return Response.status(statusCode, "Not reachable").build();
    }

}