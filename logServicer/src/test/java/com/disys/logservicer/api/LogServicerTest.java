package com.disys.logservicer.api;

import com.disys.common.DebugExceptionMapper;
import com.disys.common.model.Paths;
import org.glassfish.jersey.client.ChunkedInput;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.hamcrest.Matchers;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;

public class LogServicerTest extends JerseyTest {

    private static String logFilePath;

    @Override
    protected Application configure() {
        return new ResourceConfig(LogServicer.class, DebugExceptionMapper.class);
    }


    @BeforeClass
    public static void setup() throws IOException {
        File logFile = createTestLogFile();
        logFilePath = logFile.getAbsolutePath();
    }

    @Test
    public void whenSimpleGrep_thenReturnCorrectMatch() {
        Response response = target(Paths.LogServicer.LOG_SERVICER)
                .path(Paths.LogServicer.GREP)
                .queryParam("pattern", "becomes")
                .queryParam("fileLocation", logFilePath)
                .queryParam("grepOption", "")
                .request()
                .get();
        responseIs(response, 200);
        String responseAsString = readChunkedResponse(response);
        assertThat(responseAsString, Matchers.equalTo("becomes a mysterious interrogation after\n"));
    }

    @Test
    public void whenRegexGrep_thenReturnCorrectMatch() {
        Response response = target(Paths.LogServicer.LOG_SERVICER)
                .path(Paths.LogServicer.GREP)
                .queryParam("pattern", "scho*")
                .queryParam("fileLocation", logFilePath)
                .queryParam("grepOption", "-i")
                .request()
                .get();
        responseIs(response, 200);
        String responseAsString = readChunkedResponse(response);
        assertThat(responseAsString, Matchers.equalTo("the retiring scholar reveals to his\n"));
    }


    @Test
    public void whenMissingGrepPattern_thenFail() {
        Response response = target(Paths.LogServicer.LOG_SERVICER)
                .path(Paths.LogServicer.GREP)
                .queryParam("pattern", "")
                .queryParam("fileLocation", logFilePath)
                .queryParam("grepOption", "-i")
                .request()
                .get();
        responseIs(response, 500);
        assertThat(response.getStatusInfo().getReasonPhrase(), Matchers.containsString("Grep pattern must be supplied"));
        response.close();
    }

    private void responseIs(Response response, int responseCode) {
        assertThat(response.getStatus(), Matchers.is(responseCode));
    }

    private static File createTestLogFile() throws IOException {
        File tempFile = File.createTempFile("LogServicer", "Test");
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(tempFile))) {
            bufferedWriter.write("An impromptu goodbye party for Professor John Oldman\n");
            bufferedWriter.write("becomes a mysterious interrogation after\n");
            bufferedWriter.write("the retiring scholar reveals to his\n");
            bufferedWriter.write("colleagues he has a longer and stranger\n");
            bufferedWriter.write("past than they can imagine - The Man from Earth\n");
        }
        return tempFile;
    }

    private String readChunkedResponse(Response response) {
        StringBuilder stringBuilder = new StringBuilder();
        try (ChunkedInput<String> chunkedInput = response.readEntity(new GenericType<ChunkedInput<String>>() {
        })) {
            String line;
            while ((line = chunkedInput.read()) != null) {
                stringBuilder.append(line);
            }
        }
        return stringBuilder.toString();
    }

}