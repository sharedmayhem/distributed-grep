package com.disys.logservicer.service;

import com.disys.common.ArgumentMapBuilder;
import com.disys.common.HttpClient;
import com.disys.common.model.Paths;
import org.apache.logging.log4j.jul.ApiLogger;

import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class HeartbeatPublisher {
    private List<URI> hostServicers;
    private HttpClient httpClient;
    private static final Logger LOGGER = ApiLogger.getLogger(HeartbeatPublisher.class.getName());

    public HeartbeatPublisher(List<URI> hostServicers) {
        this(hostServicers, new HttpClient());
    }

    HeartbeatPublisher(List<URI> hostServicers, HttpClient httpClient) {
        this.hostServicers = hostServicers;
        this.httpClient = httpClient;
    }

    public void start() {
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(1);
        executorService.scheduleAtFixedRate(this::publishHeartbeat, 0, 5, TimeUnit.SECONDS);
    }

    void publishHeartbeat() {
        Map<String, List<String>> args = ArgumentMapBuilder.start().add("url", Paths.LogServicer.BASE.toASCIIString()).build();
        hostServicers.stream().map(uri -> httpClient.post(
                uri,
                args,
                ex -> Response.status(Response.Status.BAD_REQUEST.getStatusCode(), ex.getMessage()).build(),
                Paths.HostServicer.HOST_SERVICER,
                Paths.HostServicer.HEARTBEAT
        )).forEach(response -> {
            try {
                if (response.getStatus() != 200 && response.getStatus() != 204) {
                    LOGGER.severe(String.format("Could not publish heartbeat because: Status code: %d  Status reason: %s", response.getStatus(), response.getStatusInfo().getReasonPhrase()));
                }
            } finally {
                response.close();
            }
        });
    }
}
