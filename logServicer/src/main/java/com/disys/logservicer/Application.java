package com.disys.logservicer;

import com.disys.common.DebugExceptionMapper;
import com.disys.common.GenericApplication;
import com.disys.common.model.Paths;
import com.disys.logservicer.api.LogServicer;
import com.disys.logservicer.service.HeartbeatPublisher;
import org.apache.logging.log4j.jul.ApiLogger;
import org.glassfish.grizzly.http.server.HttpServer;

import java.io.IOException;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class Application {

    private static final Logger LOGGER = ApiLogger.getLogger(Application.class.getName());

    public static void main(String[] args) {
        try {
            new HeartbeatPublisher(getHeartbeatTargets(args)).start();
            HttpServer httpServer = new GenericApplication().get(Paths.LogServicer.BASE, LogServicer.class, DebugExceptionMapper.class);
            LOGGER.info("Server started at: " + Paths.LogServicer.BASE);
            httpServer.start();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Could not start the server", e);
        }
    }

    private static List<URI> getHeartbeatTargets(String[] args) {
        if(args==null || args.length == 0) {
            LOGGER.severe("Provide at least one base URL of a host servicer!");
            System.exit(-1);
        }
        return Arrays.stream(args)
                .map(URI::create)
                .collect(Collectors.toList());
    }

}
