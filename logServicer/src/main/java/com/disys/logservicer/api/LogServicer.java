package com.disys.logservicer.api;

import com.disys.commandservicer.CommandServicer;
import com.disys.common.IOStreamHelper;
import com.disys.common.model.Paths;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.function.Function;

import static com.disys.common.Validator.noBlanks;


@Path(Paths.LogServicer.LOG_SERVICER)
@Produces(MediaType.TEXT_PLAIN)
@Consumes(MediaType.TEXT_PLAIN)
public class LogServicer {

    @GET
    @Path(Paths.LogServicer.GREP)
    public Response grep(@QueryParam("pattern") String pattern,
                         @QueryParam("fileLocation") String fileLocation,
                         @QueryParam("grepOption") List<String> grepOptions) {
        noBlanks().apply(() -> "Grep pattern must be supplied", pattern);
        noBlanks().apply(() -> "File location must be specified", fileLocation);

        return Response.ok(getStreamingOutput(() -> new CommandServicer().grep(pattern, fileLocation, grepOptions))).header("source", Paths.LogServicer.BASE.getHost()).build();
    }

    private StreamingOutput getStreamingOutput(CommandStream<InputStream> commandStream) {
        return output -> {
            try {
                IOStreamHelper.inToOut(commandStream.exec(), output, Function.identity());
            } finally {
                output.flush();
                output.close();
            }
        };
    }


    interface CommandStream<T> {
        T exec() throws IOException;
    }

}
