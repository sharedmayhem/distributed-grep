package com.disys.common;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.jul.ApiLogger;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.logging.Level;
import java.util.logging.Logger;

@Provider
public class DebugExceptionMapper implements ExceptionMapper<Exception> {

    private static final Logger LOGGER = ApiLogger.getLogger(DebugExceptionMapper.class.getName());

    @Override
    public Response toResponse(Exception exception) {
        LOGGER.log(Level.SEVERE, "Exception: ", exception);
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), ExceptionUtils.getStackTrace(exception)).build();
    }
}