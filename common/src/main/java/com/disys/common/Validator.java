package com.disys.common;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.function.Supplier;

public interface Validator {

    void apply(Supplier<String> errorMessageSupplier, String... values);

    static Validator noBlanks() {
        return (errorMessageSupplier, values) -> {
            if (Arrays.stream(values).anyMatch(StringUtils::isBlank)) {
                throw new IllegalArgumentException(errorMessageSupplier.get());
            }
        };
    }
}
