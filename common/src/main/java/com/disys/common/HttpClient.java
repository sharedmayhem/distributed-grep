package com.disys.common;

import org.glassfish.jersey.client.ClientConfig;

import javax.ws.rs.client.*;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public class HttpClient {
    private Client client;

    public HttpClient() {
        this(ClientBuilder.newClient(new ClientConfig()));
    }

    public HttpClient(Client client) {
        this.client = client;
    }

    public Response request(URI uri, Map<String, List<String>> arguments, Function<Exception, Response> onFailure, String... paths) {
        try {
            WebTarget webTarget = client.target(uri);

            for (String path : paths) {
                webTarget = webTarget.path(path);
            }

            for (Map.Entry<String, List<String>> entry : arguments.entrySet()) {
                webTarget = webTarget.queryParam(entry.getKey(), (Object[]) entry.getValue().toArray());
            }

            Invocation.Builder invocationBuilder = webTarget.request(MediaType.TEXT_PLAIN_TYPE);
            return invocationBuilder.get();
        } catch (Exception e) {
            return onFailure.apply(e);
        }
    }

    public Response post(URI uri, Map<String, List<String>> arguments, Function<Exception, Response> onFailure, String... paths) {
        try {
            WebTarget webTarget = client.target(uri);

            for (String path : paths) {
                webTarget = webTarget.path(path);
            }

            Form form = new Form();
            for (Map.Entry<String, List<String>> entry : arguments.entrySet()) {
                if (entry.getValue().size() > 0) form.param(entry.getKey(), entry.getValue().get(0));
            }

            Invocation.Builder invocationBuilder = webTarget.request(MediaType.TEXT_PLAIN_TYPE);

            return invocationBuilder.post(Entity.form(form));
        } catch (Exception e) {
            return onFailure.apply(e);
        }
    }




}
