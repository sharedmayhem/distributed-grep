package com.disys.common.model;

import java.net.URI;

public class Target {
    private URI baseURI;


    private Target(URI baseURI) {
        this.baseURI = baseURI;
    }

    public static Target target(URI baseURI) {
        return new Target(baseURI);
    }

    public static Target target(String baseURI) {
        return new Target(URI.create(baseURI));
    }

    public String host() {
        return baseURI.getHost();
    }

    public int port() {
        return baseURI.getPort();
    }

    public URI baseURI() {
        return baseURI;
    }
}
