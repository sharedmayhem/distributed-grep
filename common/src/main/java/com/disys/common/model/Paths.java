package com.disys.common.model;

import com.disys.common.NetworkHelper;

import java.net.URI;

public class Paths {

    private static final String URL_TEMPLATE = "http://%s:%d";

    public static class HostServicer {
        public static final URI BASE = get(8090);
        public static final String HOST_SERVICER = "hostservicer";
        public static final String GREP = "grep";
        public static final String HEARTBEAT = "heartbeat";
        public static final String LIST = "list";
    }

    public static class LogServicer {
        public static final URI BASE = get(8080);
        public static final String LOG_SERVICER = "logservicer";
        public static final String GREP = "grep";
    }

    private static URI get(int port) {
        return URI.create(String.format(URL_TEMPLATE, NetworkHelper.getHostname(), port));
    }

}
