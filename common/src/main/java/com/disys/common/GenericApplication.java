package com.disys.common;

import org.apache.logging.log4j.jul.ApiLogger;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;

import java.net.URI;
import java.util.Arrays;
import java.util.logging.Level;

import static org.glassfish.jersey.logging.LoggingFeature.Verbosity.PAYLOAD_ANY;

public class GenericApplication {


    public HttpServer get(URI baseURI, Class... resources) {
        return GrizzlyHttpServerFactory.createHttpServer(baseURI, create(resources), false);
    }


    private static ResourceConfig create(Class[] resources) {
        ResourceConfig resourceConfig = new ResourceConfig().registerClasses(resources);
        Arrays.stream(resources).forEach(clazz -> resourceConfig.registerInstances(new LoggingFeature(ApiLogger.getLogger(clazz.getName()), Level.INFO, PAYLOAD_ANY, 8000)));
        return resourceConfig;
    }
}
