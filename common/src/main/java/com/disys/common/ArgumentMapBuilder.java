package com.disys.common;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ArgumentMapBuilder {

    private Map<String, List<String>> argumentMap = new HashMap<>();

    public static ArgumentMapBuilder start() {
        return new ArgumentMapBuilder();
    }

    public ArgumentMapBuilder add(String key, String... values) {
        argumentMap.put(key, nonBlankValues(Arrays.asList(values)));
        return this;
    }

    private List<String> nonBlankValues(List<String> values) {
        return (values).stream().filter(StringUtils::isNotBlank).collect(Collectors.toList());
    }

    public ArgumentMapBuilder add(String key, List<String> values) {
        argumentMap.put(key, nonBlankValues((values)));
        return this;
    }

    public Map<String, List<String>> build() {
        return argumentMap;
    }
}
