package com.disys.common;

import java.io.*;
import java.util.function.Function;

public class IOStreamHelper {

    public static void inToOut(InputStream inputStream, OutputStream outputStream, Function<String, String> inputTransformer) {
        try(BufferedReader reader= new BufferedReader(new InputStreamReader(inputStream))){
            String line;
            while((line = reader.readLine())!=null) {
                outputStream.write(inputTransformer.apply(line).getBytes());
                outputStream.write("\n".getBytes());
            }
        } catch (IOException e) {
            try {
                outputStream.write(inputTransformer.apply("Exception: " + e.getMessage()).getBytes());
            } catch (IOException e1) {
                throw new RuntimeException(e1);
            }
        }
    }

}
