package com.disys.common;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;

public class NetworkHelper {
    public static String getHostname(){
        try {
            return InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            try {
                return InetAddress.getLocalHost().getHostAddress();
            } catch (UnknownHostException e1) {
                throw new RuntimeException(e1);
            }
        }
    }

    public static PortContainer getFreePort(){
        try {
            ServerSocket serverSocket = new ServerSocket(0);
            serverSocket.setReuseAddress(true);
            return new PortContainer(serverSocket);
        } catch (IOException e) {
            throw new RuntimeException("Could not get a free port!",e);
        }
    }

    public static class PortContainer {

        private ServerSocket serverSocket;

        PortContainer(ServerSocket serverSocket) {
            this.serverSocket = serverSocket;
        }

        public int port() throws IOException {
            int port = serverSocket.getLocalPort();
            serverSocket.close();
            return port;
        }
    }
}
