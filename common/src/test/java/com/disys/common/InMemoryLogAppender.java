package com.disys.common;

import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;

import java.util.ArrayList;
import java.util.List;

@Plugin(name="InMemory", category="Core", elementType="appender")
public class InMemoryLogAppender extends AbstractAppender {
    public static final List<String> messages = new ArrayList<>();

    @PluginFactory
    public static InMemoryLogAppender InMemoryLogAppender(@PluginAttribute("name") String name) {
        return new InMemoryLogAppender(name);
    }

    private InMemoryLogAppender(String name) {
        super(name, null, null);
    }

    public void append(LogEvent event) {
        messages.add(event.getMessage().getFormattedMessage());
    }

    public static String lastLog() {
        return messages.get(messages.size()-1);
    }


}
